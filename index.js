const express = require('express');
const bodyParser = require('body-parser');

const serverClass = require('./server')

require('dotenv').config()
const PORT = process.env.PORT || 8080
const IP = '127.0.0.1'

const serverInit = new serverClass(PORT, IP)